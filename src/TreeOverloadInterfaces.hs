{-# LANGUAGE MultiParamTypeClasses #-}
module TreeOverloadInterfaces where


class TreeHeight tree where
    treeHeight :: tree -> Int