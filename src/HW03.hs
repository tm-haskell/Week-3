module HW03 where
import BinTree
import OrdTree
import TreeOverloadInterfaces

dfsTree ::  [OrdTree a] -> [a]
dfsTree trees = case (trees) of
    [] -> []
    trees -> concatMap flattenDfs trees

-- traverses tree in postorder (LRN), adding all elements to the result list
flattenDfs :: OrdTree a -> [a]
flattenDfs (OrdTree val children) = 
    concatMap flattenDfs children ++ [val]

-- bfs tree is a part of ord tree module 
-- (i needed to put it there because 
-- I started getting cycle dependencies between modules at some point)

equalTree :: (Eq a) => OrdTree a -> OrdTree a -> Bool
equalTree left right = (flattenBfs left) == (flattenBfs right)

-- TODO
toBinTree :: [OrdTree a] -> BinTree a
toBinTree = undefined
-- TODO
toOrdTree :: BinTree a -> [OrdTree a]  
toOrdTree = undefined

-- no duplicates allowed
isSearch :: (Ord a) => BinTree a -> Bool
isSearch node = case (node) of 
    EmptyTree -> True
    Node _ EmptyTree EmptyTree -> True
    Node val left right -> 
        greaterThanLeft && lessThanRight &&
            isSearch left && isSearch right 
                where
                    greaterThanLeft = isEmptyBinTree left || val > binTreeVal left
                    lessThanRight = isEmptyBinTree right || val < binTreeVal right

elemSearch ::(Ord a) => BinTree a -> a -> Bool
elemSearch node el = case (node, el) of
    (EmptyTree, _) -> False
    (Node nodeVal left right, val)
        | valueGreater -> elemSearch right val
        | valueLess -> elemSearch left val
        | otherwise -> True
            where
                valueGreater = val > nodeVal
                valueLess = val < nodeVal

-- does nothing if the element already present
insSearch :: (Ord a) => BinTree a -> a -> BinTree a
insSearch EmptyTree el = leaf el
insSearch node@(Node val left right) el
    | el < val = addLeftChild node (insSearch left el)
    | el > val = addRightChild node (insSearch right el)
    | otherwise = node


delSearch :: (Ord a) => BinTree a -> a -> BinTree a
delSearch node el = case (node, el) of
    (EmptyTree, _) -> EmptyTree
    (node@(Node nodeVal left right), el)
        | valueGreater -> addRightChild node (delSearch right el)
        | valueLess -> addLeftChild node (delSearch left el)
        | otherwise -> delNode node
            where
                valueGreater = el > nodeVal
                valueLess = el < nodeVal

-- returns the tree without the root node
-- elevates the higher subtree, preserving balance 
delNode :: (Ord a) => BinTree a -> BinTree a
delNode EmptyTree = EmptyTree
delNode node@(Node _ left right)
    | isChildlessNode node = EmptyTree
    | isEmptyBinTree left = elevateRight
    | isEmptyBinTree right = elevateLeft
    | otherwise =
        if treeHeight left > treeHeight right then
            elevateLeft else elevateRight
                where
                    elevateLeft = addRightChild left right
                    elevateRight = addLeftChild right left

sortList :: (Ord a) => [a] -> [a]
sortList list = inorderBin listTree
    where 
        listTree = foldl insSearch EmptyTree list


-- ord tree defs ---------------------------------



