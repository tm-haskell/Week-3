{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module BinTree where
import OrdTree
import TreeOverloadInterfaces

data BinTree a = 
    EmptyTree
    | Node {
        binTreeVal :: a,
        binTreeLeft :: BinTree a,
        binTreeRight :: BinTree a
    }
    deriving (Show, Eq)

isEmptyBinTree :: BinTree a -> Bool
isEmptyBinTree tree = case (tree) of
    EmptyTree -> True
    _ -> False

isChildlessNode :: BinTree a -> Bool
isChildlessNode node = case (node) of
    (Node _ EmptyTree EmptyTree) -> True
    _ -> False

leaf :: a -> BinTree a
leaf val = Node val EmptyTree EmptyTree

bothNodes :: BinTree a -> [BinTree a]
bothNodes node = case (node) of
    EmptyTree -> []
    (Node _ EmptyTree EmptyTree) -> [] 
    (Node _ EmptyTree b) -> [b]
    (Node _ a EmptyTree) -> [a]
    (Node _ a b) -> [a,b]

addLeftChild :: BinTree a -> BinTree a -> BinTree a
addLeftChild EmptyTree _ = error "cannot add child to empty tree"
addLeftChild (Node val _ right) newLeft =
    Node val newLeft right

addRightChild :: BinTree a -> BinTree a -> BinTree a
addRightChild EmptyTree _ = error "cannot add child to empty tree"
addRightChild (Node val left _) newRight =
    Node val left newRight


bfsBin :: BinTree a -> [a]
bfsBin tree = fbb [tree]
    where
        fbb [] = []
        fbb xs = map binTreeVal xs ++ fbb (concat (map bothNodes xs))

inorderBin :: BinTree a -> [a]
inorderBin tree = case (tree) of
    EmptyTree -> []
    Node val left right -> 
        inorderBin left ++ (val : inorderBin right)

-- flattenInorderBin :: BinTree a -> [a]

-- binTreefromList :: (Ord a) => [a] -> BinTree a
-- binTreefromList list = foldr bstAdd (EmptyTree :: BinTree a) list  


-- adds element to the shallowmost position
-- passed bintree must be balanced and a binary search tree, else exception is thrown
-- for future: implementation here:
-- https://stackoverflow.com/questions/16157203/build-balanced-binary-tree-with-foldr
--addBalanced = undefined


-- helpers
binTreesEqual :: (Eq a) => BinTree a -> BinTree a -> Bool
binTreesEqual left right = (bfsBin left) == (bfsBin right)

equivalentToOrdTree :: (Eq a) => BinTree a -> OrdTree a -> Bool
equivalentToOrdTree bin rose = 
    (bfsBin bin) == (flattenBfs rose)

instance TreeHeight (BinTree a) where
    treeHeight x = case (x) of
        EmptyTree -> 0
        Node val left right -> 
            (max (treeHeight left) (treeHeight right)) + 1

isBalancedTree :: BinTree a -> Bool
isBalancedTree tree = case (tree) of
    (EmptyTree) -> True
    (Node _ EmptyTree EmptyTree) -> True
    (Node _ l r) ->  
        let diff = abs (treeHeight l - treeHeight r) in
            diff <= 1 && isBalancedTree l && isBalancedTree r

showBinTree :: (Show a) => BinTree a -> String
showBinTree root = case (root) of
    EmptyTree -> "Empty Tree"
    _ -> showBinTreeHelper root 0 '#'

showBinTreeHelper :: (Show a) => BinTree a -> Int -> Char -> [Char]
showBinTreeHelper EmptyTree _ arrow = []
showBinTreeHelper (Node val left right) layer arrow =
    showBinTreeHelper right (layer + 1) '/'++
    "\n" ++ tabs ++ dashes ++ show val ++
        (showBinTreeHelper left (layer + 1) '\\')
        where
            tabs = 
                let tabLength = if layer < 2 then 0 else (layer - 1) in
                    (take tabLength (repeat '\t'))
            dashes = if layer < 1 then [] else arrow : "---"


---------------------- examples -----------------------------------------
-- equivivalent to one layer rose tree
smallBinTreeEx :: BinTree Int
smallBinTreeEx = 
    Node 0 
        (Node 1 
            (Node 2
                (leaf 7) 
                EmptyTree)
            (leaf 3)
        )
        (Node 5
            (leaf 4)
            (leaf 6))

-- equivalent to deep rose tree
broadBinTreeEx :: BinTree Int
broadBinTreeEx = 
    Node 0 
        (Node 1 
            (Node 2
                (leaf 10) 
                (leaf 11))
            (Node 3 
                (leaf 12)
                EmptyTree)
        )
        (Node 4
            (leaf 5)
            (leaf 6 ))
