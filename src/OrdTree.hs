module OrdTree where

data OrdTree a = OrdTree {
    ordTreeVal :: a, 
    ordTreeChildren ::  [OrdTree a]
} deriving (Show)

-- this signature is part of tasks
bfsTree :: [OrdTree a] -> [a]
bfsTree trees = case (trees) of
    [] -> []
    trees -> 
        let thisLayerVals = map ordTreeVal trees
            nextLayerNodes = concatMap ordTreeChildren trees in
                thisLayerVals ++ bfsTree nextLayerNodes

flattenBfs :: OrdTree a -> [a]
flattenBfs (OrdTree val children) = val : bfsTree children










------------------------------------------------------------




-- my examples
deeperRoseTreeEx :: OrdTree Int
deeperRoseTreeEx = OrdTree 0 deeperRoseTreeForest

oneLevelRoseTreeEx :: OrdTree Int 
oneLevelRoseTreeEx = OrdTree 0 oneLevelRoseTreeForest


deeperRoseTreeForest = 
        [ OrdTree 1 [
                OrdTree 2 [], 
                OrdTree 3 [
                    OrdTree 10 [
                        OrdTree 11 [],
                        OrdTree 12 []
                    ]] ] ,
            OrdTree 4 [   
                OrdTree 5 [], 
                OrdTree 6 [] ]
        ] 

oneLevelRoseTreeForest :: [OrdTree Int]
oneLevelRoseTreeForest = 
    [   OrdTree 1 [
            OrdTree 2 [], 
            OrdTree 3 [],
            OrdTree 4 [] ],
        OrdTree 5 [   
            OrdTree 6 [], 
            OrdTree 7 [] ]
    ] 