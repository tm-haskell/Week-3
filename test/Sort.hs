module Sort where

import Test.Hspec
import TestTemplate
import HW03
import Data.List

sortTests = hspec $ do
    describe "Sort" $ do
        subTest "Sort empty case"
            (sortTestCode [])

        subTest "Sort single element case"
            (sortTestCode [42])
        
        subTest "Sort general case"
            (sortTestCode [42, 13, 17, 250, 2, 3])

sortTestCode :: [Int] -> Expectation
sortTestCode xs = sortList xs `shouldBe` sort xs
    