module Equal where

import Test.Hspec
import TestTemplate
import HW03
import OrdTree
        
equalTests = hspec $ do
    describe "OrdTree Equal" $ do

        subTest "same"
            (equalTree deeperRoseTreeEx deeperRoseTreeEx `shouldBe` True)

        subTest "different"
            (equalTree deeperRoseTreeEx oneLevelRoseTreeEx `shouldBe` False)





