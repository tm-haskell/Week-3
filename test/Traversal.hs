module Traversal where

import Test.Hspec
import TestTemplate
import HW03
import OrdTree
        
traversalTests = hspec $ do
    describe "DFS" $ do
        
        subTest "tree general"
            (flattenDfs deeperRoseTreeEx `shouldBe` deeperTreeDfsFlattened)

        subTest "deeper forest"
            (dfsTree deeperRoseTreeForest `shouldBe` deeperForestDfsFlattened)
        
        subTest "empty forest"
            (dfsTree ([] :: [OrdTree Int]) `shouldBe` [])

    describe "BFS" $ do

        subTest "tree one layer"
            (flattenBfs oneLevelRoseTreeEx `shouldBe` (oneLevelTreeBfsFlattened))
        
        subTest "one layer"
            (bfsTree oneLevelRoseTreeForest `shouldBe` oneLevelForestBfsFlattened)

        subTest "deeper tree"
            (bfsTree deeperRoseTreeForest `shouldBe` deeperForestBfsFlattened )


deeperForestDfsFlattened  = [2, 11, 12, 10, 3, 1, 5, 6, 4]
deeperTreeDfsFlattened = deeperForestDfsFlattened ++ [0] 
oneLevelTreeDfsFlattened = [2, 3, 4, 1, 6, 7, 5]

oneLevelForestBfsFlattened = [1, 5, 2, 3, 4, 6, 7]
oneLevelTreeBfsFlattened = [0] ++ oneLevelForestBfsFlattened

deeperForestBfsFlattened = [1, 4, 2, 3, 5, 6, 10, 11, 12]