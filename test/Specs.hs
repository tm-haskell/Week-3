module Main  where
    
import Traversal
import Equal
import BinTreeTests
import BinarySearchTree
import BinTree
import Sort


main :: IO ()
main = all_tests

all_tests :: IO()
all_tests = do 
    -- traversalTests 
    -- equalTests
    -- binTreeTests
    -- bstTests
    sortTests
   
