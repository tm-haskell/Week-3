module BinarySearchTree where

import Test.Hspec
import TestTemplate
import BinTree
import HW03
import Control.Exception
import Debug.Trace
        
bstTests = hspec $ do
    describe "IsBinarySearchTree" $ do

        subTest "empty -> true"
            (isSearch (EmptyTree :: BinTree Int) `shouldBe` True)

        subTest "single leaf -> true"
            (isSearch (leaf 0) `shouldBe` True)

        subTest "small non-search tree"
            (isSearch smallBinTreeEx `shouldBe` False)

        subTest "broad non-search tree"
            (isSearch smallBinTreeEx `shouldBe` False)
        
        subTest "true BST"
            (isSearch trueBst `shouldBe` True)

    describe "ElemSearch" $ do
        
        subTest "element there"
            (elemSearch trueBst 17 `shouldBe` True)

        subTest "element missing"
            (elemSearch trueBst 404 `shouldBe` False)

        subTest "empty tree"
            (elemSearch EmptyTree 0 `shouldBe` False)

    describe "Insert" $ do

        subTest "larger element to leaf"
            (insSearch (leaf 1) 2 `shouldBe` (Node 1 EmptyTree (leaf 2)))

        subTest "smaller element to leaf"
            (insSearch (leaf 1) 0 `shouldBe` (Node 1 (leaf 0) EmptyTree))

        subTest "after leaf"
            (insSearch trueBst 9 `shouldSatisfy` (binTreesEqual trueBst9Inserted))

        subTest "does nothing if element already present"
            (insSearch trueBst 51 `shouldSatisfy` (binTreesEqual trueBst))

    describe "Delete" $ do

        subTest "from leaf"
            ((delSearch (Node 1 (leaf 0) (leaf 2))) 0 `shouldBe` (Node 1 EmptyTree (leaf 2)))
        
        subTest "picks the subtree with least height for substituting the deleted node"
            (delSearch deletionBalancedBST 0 `shouldBe` deletionBalancedBSTRootDeleted)

        subTest "general"
            (delSearch trueBst 13 `shouldBe` trueBst13Deleted)

        subTest "result is a BST"
            (delSearch trueBst 13 `shouldSatisfy` isSearch)


            ------------ examples --------------------------------------------------------------------------------------
trueBst = 
    Node 6
        (leaf 1)
        (Node 42
            (Node 13
                (leaf 8)
                (leaf 17))
            ((Node 51)
                EmptyTree
                (leaf 52)))

trueBst9Inserted = 
    Node 6
        (leaf 1)
        (Node 42
            (Node 13
                (Node 8
                    EmptyTree
                    (leaf 9))
                (leaf 17))
            ((Node 51)
                EmptyTree
                (leaf 52)))

trueBst13Deleted = 
    Node 6
        (leaf 1)
        (Node 42
            (Node 17
                (leaf 8)
                EmptyTree)
            ((Node 51)
                EmptyTree
                (leaf 52)))

deletionBalancedBST = 
    Node 0
        (leaf (-1))
        (Node 1
            EmptyTree
            (leaf 2)    
        )

deletionBalancedBSTRootDeleted = 
    Node 1
        (leaf (-1))
        (leaf 2)


oneChildRoot = 
    (Node 0
        (leaf 1)
        EmptyTree
    )

twoChildRoot = 
    (Node (0::Int)
        (leaf 1)
        (leaf 2))
        