module BinTreeTests where

import Test.Hspec
import TestTemplate
import BinTree
import OrdTree
import TreeOverloadInterfaces
import Control.Exception
        
binTreeTests = hspec $ do
    describe "Height" $ do

        subTest "empty has treeHeight of 0"
            (treeHeight EmptyTree `shouldBe` 0)

        subTest "childless root has treeHeight of 1"
            (treeHeight (leaf 0) `shouldBe` 1)

        subTest "small tree"
            (treeHeight smallBinTreeEx `shouldBe` smallerBinTreeExHeight)

        subTest "broader tree"
            (treeHeight broadBinTreeEx `shouldBe` broadBinTreeExHeight)

    describe "FlattenBfs" $ do

        subTest "general"
            (bfsBin smallBinTreeEx `shouldBe` smallerBinTreeBfsFlattened)

    describe "equalToOrdTree" $ do
    
        subTest "small tree"
            ((equivalentToOrdTree smallBinTreeEx oneLevelRoseTreeEx) `shouldBe` True)

        subTest "small tree"
            ((equivalentToOrdTree broadBinTreeEx deeperRoseTreeEx) `shouldBe` True)

    describe "IsBalanced" $ do
        
        subTest "balanced"
            ((isBalancedTree (Node 0 (leaf 1) EmptyTree)) `shouldBe` True)

        subTest "unbalanced"
            (isBalancedTree notBalancedBinTree `shouldBe` False)

        subTest "empty"
            (isBalancedTree EmptyTree `shouldBe` True)
    
    describe "Inorder" $ do
        subTest "general"
            (inorderBin smallBinTreeEx `shouldBe` smallerBinTreeInorderFlattened)

    


----------------- examples ----------------------------------------------------------------------------------------------
smallerBinTreeExHeight =  (4 :: Int)
broadBinTreeExHeight =  (4 :: Int)

smallerBinTreeBfsFlattened = [0, 1, 5, 2, 3, 4, 6, 7]
smallerBinTreeInorderFlattened = [7, 2, 1, 3, 0, 4, 5, 6]

notBalancedBinTree = 
    (Node (0::Int) 
        (Node 1 (leaf 2)
                EmptyTree)
        EmptyTree)
